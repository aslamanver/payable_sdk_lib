package com.payable.sdk;

public interface PayableListener {
    void onPaymentSuccess(Payable payable);
    void onPaymentFailure(Payable payable);
}